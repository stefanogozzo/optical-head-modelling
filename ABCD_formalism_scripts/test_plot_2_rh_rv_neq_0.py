# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 23:07:01 2021

@author: stefa
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm #colormaps
from matplotlib import colors

from compute_vout_hv import vout_h,vout_v

# inspect r_h!=0, r_v!=0, alpha=0, beta=0 

#create an horizontal + vertical offset grid

n_grid = 60
n_grid +=1

misal_grid = np.ones((n_grid,n_grid,2))

# for i in range(n_grid):
    
#     r_h_in = (i-((n_grid-1)/2)) / 15.
#     voh = vout_h(np.array([r_h_in,0]))
#     vov = vout_v(np.array([r_h_in,0]))
    
#     r_h_out1[i] = voh[0,0]
#     r_v_out1[i] = vov[0,0]

mhv = np.zeros(2)

for i in range(n_grid):
    
    mhv[0] = (i-((n_grid-1)/2)) / 20.
    
    for j in range(n_grid):
        
        mhv[1] = (j-((n_grid-1)/2)) / 20.
        
        misal_grid[i,j,:] = mhv

colors_grid = np.zeros((n_grid,n_grid,3))
    
colors_grid[:,:,0:2] = (misal_grid + abs(misal_grid[0,0,0]))/ (2*abs(misal_grid[0,0,0]))

#%%

# Plot all the offsetted BEAM 1 on the input surface 


#create a figure that correspond to the exit surface and plot the exit beams
# as a function of the misallignment

fig, axs = plt.subplots(1, 2, figsize=(17,8))
fig.suptitle(r'$r_h \neq 0$, $r_v \neq 0$, $\alpha = 0$, $\beta = 0$', fontsize=25)

x_in1 = 2.5
y_in1 = 7.5

x_in4 = 2.5
y_in4 = 2.5

for i in range(n_grid):
    for j in range(n_grid):
        
        x = x_in1 + misal_grid[i,j,0]
        y = y_in1 + misal_grid[i,j,1]
        c = colors_grid[i,j,:]
        
        axs[0].plot(x,y, 'o', c=c)
        
        x = x_in4 + misal_grid[i,j,0]
        y = y_in4 + misal_grid[i,j,1]
        c = colors_grid[i,j,:]
        
        axs[0].plot(x,y, 'o', c=c)
        
axs[0].set_xlim(0,10)
axs[0].set_ylim(0,10)

axs[0].text(7.5, 7.5, 'BEAM 2', c='k', fontsize=20, horizontalalignment='center', verticalalignment='center')
axs[0].text(7.5, 2.5, 'BEAM 3', c='k', fontsize=20, horizontalalignment='center', verticalalignment='center')

axs[0].set_xlabel('x [mm]', fontsize=20)
axs[0].set_ylabel('y [mm]', fontsize=20)
axs[0].set_title('Input Surface', fontsize=20)


#%%

#plot all the offsetted output BEAM 1 associated to the misal_grid inputs

#compute the output vector (expressed in output surface based FoR)

vout_grid = np.zeros((n_grid,n_grid,4,2,2))

m_vect_h = np.zeros((n_grid,n_grid,4,2))

for n in range(4):
    m_vect_h[:,:,n,0] = misal_grid[:,:,0]
    
m_vect_v = np.zeros((n_grid,n_grid,4,2))

for n in range(4):
    m_vect_v[:,:,n,0] = misal_grid[:,:,1]


for i in range(n_grid):
    for j in range(n_grid):

        vout_grid[i,j,:,0,:] = vout_h(m_vect_h[i,j,:,:])
        vout_grid[i,j,:,1,:] = vout_v(m_vect_v[i,j,:,:])
        

#fig, axs = plt.subplots(1, 1, figsize=(10,10))

for i in range(n_grid):
    for j in range(n_grid):
        
        c = colors_grid[i,j,:]
        
        axs[1].plot(vout_grid[i,j,1,0,0], vout_grid[i,j,1,1,0] , 'o', c=c)
        axs[1].plot(vout_grid[i,j,2,0,0], vout_grid[i,j,2,1,0] , 'o', c=c)
        

axs[1].set_xlim(0,10)
axs[1].set_ylim(0,10)

axs[1].text(2.5, 4.5, 'BEAM 2', c='k', fontsize=20, horizontalalignment='center', verticalalignment='center')
axs[1].text(7.5, 4.5, 'BEAM 3', c='k', fontsize=20, horizontalalignment='center', verticalalignment='center')

axs[1].set_xlabel('x [mm]', fontsize=20)
axs[1].set_ylabel('y [mm]', fontsize=20)
axs[1].set_title('Output Surface', fontsize=20)



