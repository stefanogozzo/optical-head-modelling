# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 17:48:45 2021

@author: stefa
"""

#       This function accepts as input the 1x2 input vector (v_i)
#       expressed in input surface FoR coordinates
#       and returns the output vector (v_o) expressed in output surface FoR coords


# INPUT: np.array((4,2))
#       if misal_vect == False:
    #       input vector expressed in the Input Surface coordinates
#       if misal_vect == True:
    #       input misalignment vector ( v_in - v_axis ) in Input Surface coords 
    
# OUTPUT: np.array((4,2))
#       the 4 output vectors v_out expressed in the Output Surface coordinates


# NOTE: (see fig 2.2, pag. 10)

#       The output beam will be given in the output surface based frame of reference.
       
#       The input beam is instead given in the input surface based FoR.

#       Then, the general equation to obtain the output ray vector, given the input ray, is:
    
#       / r_o \  =  / r_axis_out \  +  / A B \ x  (  / r_i \ - / r_axis_in \ )
#       \ a_o /     \      0     /     \ C D /    (  \ a_i /   \     0     / )


# See Ch.2 for a detailed explanation of this script


import numpy as np


def vout_h(v_i, misal_vect=True):

    # import the complete abcd matrices for the 4 beam paths
    from abcd_compl_hv import abcd_complete_h

    abcd_full = abcd_complete_h()

    
    # initialize the axis vector in input and output surface coordinates
    ao_in = np.zeros((4,2))
    ao_out = np.zeros((4,2))
    
    of1 = 2.5
    of2 = 7.5
    
    ao_in[0,:] = np.array([of1,0])
    ao_in[1,:] = np.array([of1,0])
    ao_in[2,:] = np.array([of1,0])
    ao_in[3,:] = np.array([of1,0])
    
    ao_out[0,:] = np.array([of2,0])
    ao_out[1,:] = np.array([of1,0])
    ao_out[2,:] = np.array([of2,0])
    ao_out[3,:] = np.array([of1,0])


    #compute output vectors 
    
    v_o = np.zeros((4,2))
    
    for n in range(len(v_o)):
        
        if misal_vect == False:
            v_o[n,:] = ao_out[n,:] + np.matmul(abcd_full[n,:,:], (v_i[n,:]-ao_in[n,:]) )
        if misal_vect == True:
            v_o[n,:] = ao_out[n,:] + np.matmul(abcd_full[n,:,:], (v_i[n,:]) )

    return v_o



def vout_v(v_i, misal_vect=True):

    # import the complete abcd matrices for the 4 beam paths
    from abcd_compl_hv import abcd_complete_v

    abcd_full = abcd_complete_v()

    
    # initialize the axis offset values for input and output surfaces FoRs
    ao_in = np.zeros((4,2))
    ao_out = np.zeros((4,2))
    
    of1 = 2.5
    of2 = 7.5
    
    ao_in[0,:] = np.array([of2,0])
    ao_in[1,:] = np.array([of2,0])
    ao_in[2,:] = np.array([of1,0])
    ao_in[3,:] = np.array([of1,0])
    
    ao_out[0,:] = np.array([of2,0])
    ao_out[1,:] = np.array([of2,0])
    ao_out[2,:] = np.array([of2,0])
    ao_out[3,:] = np.array([of2,0])


    #compute output vectors 
    
    v_o = np.zeros((4,2))
    
    for n in range(len(v_o)):
        
        if misal_vect == False:
            v_o[n,:] = ao_out[n,:] + np.matmul(abcd_full[n,:,:], (v_i[n,:]-ao_in[n,:]) )
        if misal_vect == True:
            v_o[n,:] = ao_out[n,:] + np.matmul(abcd_full[n,:,:], (v_i[n,:]) )

    return v_o






