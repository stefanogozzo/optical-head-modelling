# -*- coding: utf-8 -*-
"""
Created on Tue Sep 28 13:54:03 2021

@author: stefa
"""


# This script sets up and computes the complete ABCD matrices associated to the optical paths
# of each one of the 4 beams inside the TOH.

# INPUT: none
# OUTPUT: np.array((4,2,2))
            # 4 ABCD matrices associated to the full propagation of each beam
            
  
# See Ch.2 for a detailed explanation of this script

 

# NOTE: (see fig 2.2, pag. 10)

#       The output beam will be given in the output surface based frame of reference.
       
#       The input beam is instead given in the input surface based FoR.

#       Then, the general equation to obtain the output ray vector, given the input ray, is:
    
#       / r_o \  =  / r_axis_out \  +  / A B \ x  (  / r_i \ - / r_axis_in \ )
#       \ a_o /     \      0     /     \ C D /    (  \ a_i /   \     0     / )



# NOTE: The analysis is split btw the horizontal and vertical plane cases.
#       This is because the optical axis FoR gets 'flipped' at each reflection.
#       As each beam has a different n. of reflections in the horiz and vertical plane
#       the final '+/-' sign of the ABCD matrix must be computed separately in each case. 



import numpy as np


def abcd_complete_h():


    # parametrically define the matrices of standard optical elements
    
    def propag(l):
        
        return np.array([[1.,l],[0.,1.]])
    
    #reflection (flat interface)
    def rfl_fi():
        
        return np.array([[1.,0.],[0.,1.]])
    
    #refraction (flat interface)
    def rfr_fi(n1,n2):
        
        return np.array([[1.,0.],[0.,n1/n2]])
    
    
    
    # NOTE: *** APPROXIMATION *** (really bad)
    #   The QWP is considered as a piece of glass 
    
    
    # NOTA: *** ATTENTION ***
    # the following abcd matrix + vector formalism is only valid in paraxial approximation 
    # that is:  cos(alpha)=1, sin(alpha)=theta
      
    
    # store the abcd matrix associated to the complete propagation of each beam
    abcd_full = np.zeros((4,2,2)) 
    
    
    #initialize all the matrices that accounts for the correct sign in the r,alpha variables
    #after the coordinate transformation from input to output coords
    
    io_transf_h = np.zeros((4,2,2))
    
    io_transf_h[0,:,:] = np.array([[+1,0],[0,+1]])
    io_transf_h[1,:,:] = np.array([[-1,0],[0,-1]])
    io_transf_h[2,:,:] = np.array([[+1,0],[0,+1]])
    io_transf_h[3,:,:] = np.array([[-1,0],[0,-1]])
    
    
    # BEAM 1
    
    # initialize the number of optical path segments of the upper beam (beam 1+2)
    # or equvalently the number of surfaces (included input surface)
    
    n_segm = 11
    
    n_segm += 1
    
    
    # storage arrays
    
    #optical path lengths (included 'null' length input beam)
    ops = np.zeros(n_segm)
    # refraction indeces
    n_ri = np.zeros(n_segm+1)
    # surface properties
    surf_tt = np.array(np.zeros(n_segm),dtype=bool)
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    
    
    # BEAM 1
    
    # initialize optical path lengths 
    # first model: compute them for perfectly aligned input beam
    
    x_in = 2.5 #mm
    y_in = 7.5 #mm
    a30 = 30./180.*np.pi #rad
    a60 = 60./180.*np.pi #rad
    l_pbs = 10. #mm
    l_qwp = 1. #mm
    l_out = 5. #mm
    
    #NOTE: they can be better parametrized
    ops = np.array([0., x_in*np.tan(a30), 2*x_in*np.tan(a30), 2*x_in*np.cos(a30), l_pbs, l_qwp, l_qwp, y_in , l_pbs-y_in, (l_pbs-y_in)*np.tan(a60), ((l_pbs/2)-(l_pbs-y_in))/np.cos(a30) , x_in*np.tan(a30)])
    
    # initialize refraction indeces (corresponding to the optical segments (figure notation)
    # + external n)
    
    # external (air)
    n_air = 1.0
    # TOH (first approx: glass)
    n_gl = 1.5 
    # QWP fast axis (vertical)
    n_fast = 1.5
    # QWP slow axis (horizontal)
    n_slow = 1.5
    
    n_ri = np.array([n_air,n_gl,n_gl,n_gl,n_gl,n_slow,n_slow,n_gl,n_gl,n_gl,n_gl,n_gl,n_air])
    
    # initialize the surface properties:
        # TRUE = Transmission
        # FALSE = Reflection
    
    surf_tt[0] = True
    surf_tt[1] = True
    surf_tt[2] = False
    surf_tt[3] = True
    surf_tt[4] = True
    surf_tt[5] = False
    surf_tt[6] = True
    surf_tt[7] = False
    surf_tt[8] = True
    surf_tt[9] = False
    surf_tt[10] = True
    surf_tt[11] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam (accounting for the correct reflection sign)
    abcd_full[0,:,:] = np.matmul(io_transf_h[0,:,:], abcd_syst[:,:])
    
    
    
    
    # BEAM 2
    
    #the only difference is surface S1
    surf_tt[1] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam
    abcd_full[1,:,:] = np.matmul(io_transf_h[1,:,:], abcd_syst[:,:])
    
    
    
    
    
    # BEAM 3
    
    # initialize the number of optical path segments of the upper beam (beam 1+2)
    # or equvalently the number of surfaces (included input surface)
    
    n_segm = 13
    
    n_segm += 1
    
    
    # storage arrays
    
    #optical path lengths (included 'null' length input beam)
    ops = np.zeros(n_segm)
    # refraction indeces
    n_ri = np.zeros(n_segm+1)
    # surface properties
    surf_tt = np.array(np.zeros(n_segm),dtype=bool)
    
    
    # initialize optical path lengths 
    # first model: compute them for perfectly aligned input beam
    
    x_in = 2.5 #mm
    y_in = 2.5 #mm
    a30 = 30./180.*np.pi #rad
    a60 = 60./180.*np.pi #rad
    l_pbs = 10. #mm
    l_qwp = 1. #mm
    l_out = 5. #mm
    
    #NOTE: they can be better parametrized
    ops = np.array([0., x_in*np.tan(a30), 2*x_in*np.tan(a30), 2*x_in*np.cos(a30), l_pbs, l_qwp, l_out, l_out, l_qwp, y_in , l_pbs-y_in, y_in*np.tan(a60), ((l_pbs/2)-(y_in))/np.cos(a30) , x_in*np.tan(a30)])
    
    # initialize refraction indeces (corresponding to the optical segments (figure notation)
    # + external n)
    
    n_ri = np.array([n_air,n_gl,n_gl,n_gl,n_gl,n_slow,n_air,n_air,n_slow,n_gl,n_gl,n_gl,n_gl,n_gl,n_air])
    
    # initialize the surface properties:
        # TRUE = Transmission
        # FALSE = Reflection
    
    surf_tt[0] = True
    surf_tt[1] = False
    surf_tt[2] = False
    surf_tt[3] = True
    surf_tt[4] = True
    surf_tt[5] = True
    surf_tt[6] = False
    surf_tt[7] = True
    
    surf_tt[8] = True
    surf_tt[9] = False
    surf_tt[10] = True
    surf_tt[11] = False
    surf_tt[12] = False
    surf_tt[13] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam
    abcd_full[2,:,:] = np.matmul(io_transf_h[2,:,:], abcd_syst[:,:])
    
    
    
    
    # BEAM 4
    
    #the only difference is surface S1
    surf_tt[1] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam
    abcd_full[3,:,:] = np.matmul(io_transf_h[3,:,:], abcd_syst[:,:])


    return abcd_full





def abcd_complete_v():


    # parametrically define the matrices of standard optical elements
    
    def propag(l):
        
        return np.array([[1.,l],[0.,1.]])
    
    #reflection (flat interface)
    def rfl_fi():
        
        return np.array([[1.,0.],[0.,1.]])
    
    #refraction (flat interface)
    def rfr_fi(n1,n2):
        
        return np.array([[1.,0.],[0.,n1/n2]])
    
    
    
    # NOTE: *** APPROXIMATION *** (really bad)
    #   The QWP is considered as a piece of glass 
    
    
    # NOTA: *** ATTENTION ***
    # the following abcd matrix + vector formalism is only valid in paraxial approximation 
    # that is:  cos(alpha)=1, sin(alpha)=theta
      
    
    # store the abcd matrix associated to the complete propagation of each beam
    abcd_full = np.zeros((4,2,2)) 
    
    
    #initialize all the matrices that accounts for the correct sign in the r,alpha variables
    #after the coordinate transformation from input to output FoR
    
    io_transf_v = np.zeros((4,2,2))
    
    io_transf_v[0,:,:] = np.array([[-1,0],[0,-1]])
    io_transf_v[1,:,:] = np.array([[-1,0],[0,-1]])
    io_transf_v[2,:,:] = np.array([[+1,0],[0,+1]])
    io_transf_v[3,:,:] = np.array([[+1,0],[0,+1]])
    
    
    # BEAM 1
    
    # initialize the number of optical path segments of the upper beam (beam 1+2)
    # or equvalently the number of surfaces (included input surface)
    
    n_segm = 11
    
    n_segm += 1
    
    
    # storage arrays
    
    #optical path lengths (included 'null' length input beam)
    ops = np.zeros(n_segm)
    # refraction indeces
    n_ri = np.zeros(n_segm+1)
    # surface properties
    surf_tt = np.array(np.zeros(n_segm),dtype=bool)
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    
    
    # BEAM 1
    
    # initialize optical path lengths 
    # first model: compute them for perfectly aligned input beam
    
    x_in = 2.5 #mm
    y_in = 7.5 #mm
    a30 = 30./180.*np.pi #rad
    a60 = 60./180.*np.pi #rad
    l_pbs = 10. #mm
    l_qwp = 1. #mm
    l_out = 5. #mm
    
    #NOTE: they can be better parametrized
    ops = np.array([0., x_in*np.tan(a30), 2*x_in*np.tan(a30), 2*x_in*np.cos(a30), l_pbs, l_qwp, l_qwp, y_in , l_pbs-y_in, (l_pbs-y_in)*np.tan(a60), ((l_pbs/2)-(l_pbs-y_in))/np.cos(a30) , x_in*np.tan(a30)])
    
    # initialize refraction indeces (corresponding to the optical segments (figure notation)
    # + external n)
    
    # external (air)
    n_air = 1.0
    # TOH (first approx: glass)
    n_gl = 1.5 
    # QWP fast axis (vertical)
    n_fast = 1.5
    # QWP slow axis (horizontal)
    n_slow = 1.5
    
    n_ri = np.array([n_air,n_gl,n_gl,n_gl,n_gl,n_fast,n_fast,n_gl,n_gl,n_gl,n_gl,n_gl,n_air])
    
    # initialize the surface properties:
        # TRUE = Transmission
        # FALSE = Reflection
    
    surf_tt[0] = True
    surf_tt[1] = True
    surf_tt[2] = False
    surf_tt[3] = True
    surf_tt[4] = True
    surf_tt[5] = False
    surf_tt[6] = True
    surf_tt[7] = False
    surf_tt[8] = True
    surf_tt[9] = False
    surf_tt[10] = True
    surf_tt[11] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam (accounting for the correct reflection sign)
    abcd_full[0,:,:] = np.matmul(io_transf_v[0,:,:], abcd_syst[:,:])
    
    
    
    
    # BEAM 2
    
    #the only difference is surface S1
    surf_tt[1] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam
    abcd_full[1,:,:] = np.matmul(io_transf_v[1,:,:], abcd_syst[:,:])
    
    
    
    
    
    # BEAM 3
    
    # initialize the number of optical path segments of the upper beam (beam 1+2)
    # or equvalently the number of surfaces (included input surface)
    
    n_segm = 13
    
    n_segm += 1
    
    
    # storage arrays
    
    #optical path lengths (included 'null' length input beam)
    ops = np.zeros(n_segm)
    # refraction indeces
    n_ri = np.zeros(n_segm+1)
    # surface properties
    surf_tt = np.array(np.zeros(n_segm),dtype=bool)
    
    
    # initialize optical path lengths 
    # first model: compute them for perfectly aligned input beam
    
    x_in = 2.5 #mm
    y_in = 2.5 #mm
    a30 = 30./180.*np.pi #rad
    a60 = 60./180.*np.pi #rad
    l_pbs = 10. #mm
    l_qwp = 1. #mm
    l_out = 5. #mm
    
    #NOTE: they can be better parametrized
    ops = np.array([0., x_in*np.tan(a30), 2*x_in*np.tan(a30), 2*x_in*np.cos(a30), l_pbs, l_qwp, l_out, l_out, l_qwp, y_in , l_pbs-y_in, y_in*np.tan(a60), ((l_pbs/2)-(y_in))/np.cos(a30) , x_in*np.tan(a30)])
    
    # initialize refraction indeces (corresponding to the optical segments (figure notation)
    # + external n)
    
    n_ri = np.array([n_air,n_gl,n_gl,n_gl,n_gl,n_fast,n_air,n_air,n_fast,n_gl,n_gl,n_gl,n_gl,n_gl,n_air])
    
    # initialize the surface properties:
        # TRUE = Transmission
        # FALSE = Reflection
    
    surf_tt[0] = True
    surf_tt[1] = False
    surf_tt[2] = False
    surf_tt[3] = True
    surf_tt[4] = True
    surf_tt[5] = True
    surf_tt[6] = False
    surf_tt[7] = True
    
    surf_tt[8] = True
    surf_tt[9] = False
    surf_tt[10] = True
    surf_tt[11] = False
    surf_tt[12] = False
    surf_tt[13] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam
    abcd_full[2,:,:] = np.matmul(io_transf_v[2,:,:], abcd_syst[:,:])
    
    
    
    
    # BEAM 4
    
    #the only difference is surface S1
    surf_tt[1] = True
    
    
    # populate the abcd matrices storage array
    
    # ABCD matrices
    abcd_m = np.zeros([2*n_segm,2,2])
    
    for n in range(n_segm):
        
        # n segment propagation
        abcd_m[2*n,:,:] = propag(ops[n])
        
        # n surface
        if surf_tt[n] == True:
            
            abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
            
        if surf_tt[n] == False:
            
            abcd_m[2*n+1,:,:] = rfl_fi()
    
    
    # perform matrix multiplication
    # pay attention that elements must be multiplied from last to first
    
    abcd_syst = abcd_m[0,:,:]
    
    for n in range(1,2*n_segm):
        
        abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
        
    # store the full matrix for the beam
    abcd_full[3,:,:] = np.matmul(io_transf_v[3,:,:], abcd_syst[:,:])


    return abcd_full



    
    
