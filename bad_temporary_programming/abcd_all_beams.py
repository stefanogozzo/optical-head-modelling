# -*- coding: utf-8 -*-
"""
Created on Sat Sep 25 14:14:08 2021

@author: stefa
"""


#This script sets up the ABCD Matrix equation for a given optical path

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm #colormaps
from matplotlib import colors


# parametrically define the matrices of standard optical elements

def propag(l):
    
    return np.array([[1.,l],[0.,1.]])

#reflection (flat interface)
def rfl_fi():
    
    return np.array([[1.,0.],[0.,1.]])

#refraction (flat interface)
def rfr_fi(n1,n2):
    
    return np.array([[1.,0.],[0.,n1/n2]])



# NOTE: *** APPROXIMATION *** (really bad)
#   The QWP is considered as a piece of glass 


# NOTA: *** ATTENTION ***
# the following abcd matrix + vector formalism is only valid in paraxial approximation 
# that is:  cos(alpha)=1, sin(alpha)=theta


# store input/output vectors for the 4 beams
v_in = np.zeros((4,2))
v_out = np.zeros((4,2))  

# store the abcd matrix associated to the complete propagation of each beam
abcd_full = np.zeros((4,2,2)) 



# initialize initial vectors

# BEAM 1 + 2

r_0 = 0. #mm
alpha_0 = 0.1/180.*np.pi

v_in[0,:] = np.array([r_0,alpha_0])
v_in[1,:] = v_in[0,:]


# BEAM 3 + 4

r_0 = 0. #mm
alpha_0 = 0.1/180.*np.pi

v_in[2,:] = np.array([r_0,alpha_0])
v_in[3,:] = v_in[2,:]




# BEAM 1

# initialize the number of optical path segments of the upper beam (beam 1+2)
# or equvalently the number of surfaces (included input surface)

n_segm = 11

n_segm += 1


# storage arrays

#optical path lengths (included 'null' length input beam)
ops = np.zeros(n_segm)
# refraction indeces
n_ri = np.zeros(n_segm+1)
# surface properties
surf_tt = np.array(np.zeros(n_segm),dtype=bool)

# ABCD matrices
abcd_m = np.zeros([2*n_segm,2,2])



# BEAM 1

# initialize optical path lengths 
# first model: compute them for perfectly aligned input beam

x_in = 2.5 #mm
y_in = 7.5 #mm
a30 = 30./180.*np.pi #rad
a60 = 60./180.*np.pi #rad
l_pbs = 10. #mm
l_qwp = 1. #mm
l_out = 5. #mm

#NOTE: they can be better parametrized
ops = np.array([0., x_in*np.tan(a30), 2*x_in*np.tan(a30), 2*x_in*np.cos(a30), l_pbs, l_qwp, l_qwp, y_in , l_pbs-y_in, (l_pbs-y_in)*np.tan(a60), ((l_pbs/2)-(l_pbs-y_in))/np.cos(a30) , x_in*np.tan(a30)])

# initialize refraction indeces (corresponding to the optical segments (figure notation)
# + external n)

n_ri = np.array([1.,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.])

# initialize the surface properties:
    # TRUE = Transmission
    # FALSE = Reflection

surf_tt[0] = True
surf_tt[1] = True
surf_tt[2] = False
surf_tt[3] = True
surf_tt[4] = True
surf_tt[5] = False
surf_tt[6] = True
surf_tt[7] = False
surf_tt[8] = True
surf_tt[9] = False
surf_tt[10] = True
surf_tt[11] = True


# populate the abcd matrices storage array

# ABCD matrices
abcd_m = np.zeros([2*n_segm,2,2])

for n in range(n_segm):
    
    # n segment propagation
    abcd_m[2*n,:,:] = propag(ops[n])
    
    # n surface
    if surf_tt[n] == True:
        
        abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
        
    if surf_tt[n] == False:
        
        abcd_m[2*n+1,:,:] = rfl_fi()


# perform matrix multiplication
# pay attention that elements must be multiplied from last to first

abcd_syst = abcd_m[0,:,:]

for n in range(1,2*n_segm):
    
    abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
    
# store the full matrix for the beam
abcd_full[0,:,:] = abcd_syst[:,:]

# compute the exit beam vector

v_out[0,:] = np.matmul(abcd_syst,v_in[0,:])




# BEAM 2

#the only difference is surface S1
surf_tt[1] = True


# populate the abcd matrices storage array

# ABCD matrices
abcd_m = np.zeros([2*n_segm,2,2])

for n in range(n_segm):
    
    # n segment propagation
    abcd_m[2*n,:,:] = propag(ops[n])
    
    # n surface
    if surf_tt[n] == True:
        
        abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
        
    if surf_tt[n] == False:
        
        abcd_m[2*n+1,:,:] = rfl_fi()


# perform matrix multiplication
# pay attention that elements must be multiplied from last to first

abcd_syst = abcd_m[0,:,:]

for n in range(1,2*n_segm):
    
    abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
    
# store the full matrix for the beam
abcd_full[1,:,:] = abcd_syst[:,:]

# compute the exit beam vector

v_out[1,:] = np.matmul(abcd_syst,v_in[1,:])





# BEAM 3

# initialize the number of optical path segments of the upper beam (beam 1+2)
# or equvalently the number of surfaces (included input surface)

n_segm = 13

n_segm += 1


# storage arrays

#optical path lengths (included 'null' length input beam)
ops = np.zeros(n_segm)
# refraction indeces
n_ri = np.zeros(n_segm+1)
# surface properties
surf_tt = np.array(np.zeros(n_segm),dtype=bool)


# initialize optical path lengths 
# first model: compute them for perfectly aligned input beam

x_in = 2.5 #mm
y_in = 2.5 #mm
a30 = 30./180.*np.pi #rad
a60 = 60./180.*np.pi #rad
l_pbs = 10. #mm
l_qwp = 1. #mm
l_out = 5. #mm

#NOTE: they can be better parametrized
ops = np.array([0., x_in*np.tan(a30), 2*x_in*np.tan(a30), 2*x_in*np.cos(a30), l_pbs, l_qwp, l_out, l_out, l_qwp, y_in , l_pbs-y_in, y_in*np.tan(a60), ((l_pbs/2)-(y_in))/np.cos(a30) , x_in*np.tan(a30)])

# initialize refraction indeces (corresponding to the optical segments (figure notation)
# + external n)

n_ri = np.array([1.,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.])

# initialize the surface properties:
    # TRUE = Transmission
    # FALSE = Reflection

surf_tt[0] = True
surf_tt[1] = False
surf_tt[2] = False
surf_tt[3] = True
surf_tt[4] = True
surf_tt[5] = True
surf_tt[6] = False
surf_tt[7] = True

surf_tt[8] = True
surf_tt[9] = False
surf_tt[10] = True
surf_tt[11] = False
surf_tt[12] = False
surf_tt[13] = True


# populate the abcd matrices storage array

# ABCD matrices
abcd_m = np.zeros([2*n_segm,2,2])

for n in range(n_segm):
    
    # n segment propagation
    abcd_m[2*n,:,:] = propag(ops[n])
    
    # n surface
    if surf_tt[n] == True:
        
        abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
        
    if surf_tt[n] == False:
        
        abcd_m[2*n+1,:,:] = rfl_fi()


# perform matrix multiplication
# pay attention that elements must be multiplied from last to first

abcd_syst = abcd_m[0,:,:]

for n in range(1,2*n_segm):
    
    abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
    
# store the full matrix for the beam
abcd_full[2,:,:] = abcd_syst[:,:]

# compute the exit beam vector

v_out[2,:] = np.matmul(abcd_syst,v_in[2,:])




# BEAM 4

#the only difference is surface S1
surf_tt[1] = True


# populate the abcd matrices storage array

# ABCD matrices
abcd_m = np.zeros([2*n_segm,2,2])

for n in range(n_segm):
    
    # n segment propagation
    abcd_m[2*n,:,:] = propag(ops[n])
    
    # n surface
    if surf_tt[n] == True:
        
        abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
        
    if surf_tt[n] == False:
        
        abcd_m[2*n+1,:,:] = rfl_fi()


# perform matrix multiplication
# pay attention that elements must be multiplied from last to first

abcd_syst = abcd_m[0,:,:]

for n in range(1,2*n_segm):
    
    abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
    
# store the full matrix for the beam
abcd_full[3,:,:] = abcd_syst[:,:]

# compute the exit beam vector

v_out[3,:] = np.matmul(abcd_syst,v_in[3,:])

#%%

# plot some results in a pretty way

# as all this system is treated as linear, we can just inspect the v_out under 
# variation of the v_in horizontal axis, and extend the result to all directions
# (also the result under translation are trivial)

# circle1 = plt.Circle((0, 0), 0.2, color='r')
# circle2 = plt.Circle((0.5, 0.5), 0.2, color='blue')
# circle3 = plt.Circle((1, 1), 0.2, color='g', clip_on=False)

n_grid = 100
n_grid +=1

misal_grid = np.ones((n_grid,n_grid))

for i in range(n_grid):
    
    x = (i-((n_grid-1)/2)) / 20.
    
    for j in range(n_grid):
        
        y = (j-((n_grid-1)/2)) / 20.
        
        misal_grid[i,j] = np.sqrt(x**2 + y**2)
        


# map the initial beam misalignement to an output beam offset by using the abcd full matrix

# BEAM 1 and 4

max_misall = misal_grid[0,0]

r_out_1 = abcd_full[0,0,1] * misal_grid/180.*np.pi
r_out_4 = abcd_full[3,0,1] * misal_grid/180.*np.pi


#create a figure that correspond to the exit surface and plot the exit beams
# as a function of the misallignment

fig, axs = plt.subplots(1, 1, figsize=(12,10))

x_out1 = 2.5
y_out1 = 7.5

for i in range(n_grid):
    for j in range(n_grid):
        
        x = x_out1 + (i-((n_grid-1)/2)) / ((n_grid-1)/2) * r_out_1[int(((n_grid-1)/2)),0]
        y = y_out1 + (j-((n_grid-1)/2)) / ((n_grid-1)/2) * r_out_1[0,int(((n_grid-1)/2))]
        c = cm.Greens(misal_grid[i,j] / max_misall)
        
        axs.plot(x,y, 'o', c=c)

x_out2 = 7.5
y_out2 = 7.5

for i in range(n_grid):
    for j in range(n_grid):
        
        x = x_out2 + (i-((n_grid-1)/2)) / ((n_grid-1)/2) * r_out_4[int(((n_grid-1)/2)),0]
        y = y_out2 + (j-((n_grid-1)/2)) / ((n_grid-1)/2) * r_out_4[0,int(((n_grid-1)/2))]
        c = cm.Greens(misal_grid[i,j] / max_misall)
        
        axs.plot(x,y, 'o', c=c)


axs.set_xlim(0,10)
axs.set_ylim(0,10)

cbar = fig.colorbar(cm.ScalarMappable(norm=colors.Normalize(vmin=0., vmax=max(misal_grid[0,:])), cmap='Greens') ,ax= axs)
cbar.set_label(r'|$\alpha$| [deg]', fontsize=20)

axs.text(2.5, 4.5, 'BEAM 1', c='r', fontsize=20, horizontalalignment='center', verticalalignment='center')
axs.text(7.5, 4.5, 'BEAM 4', c='r', fontsize=20, horizontalalignment='center', verticalalignment='center')

axs.set_xlabel('x [mm]', fontsize=20)
axs.set_ylabel('y [mm]', fontsize=20)



