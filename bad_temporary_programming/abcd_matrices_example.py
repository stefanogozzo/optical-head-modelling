# -*- coding: utf-8 -*-
"""
Created on Fri Sep 24 15:18:24 2021

@author: stefa
"""

#This script sets up the ABCD Matrix equation for a given optical path

import numpy as np


# parametrically define the matrices of standard optical elements

def propag(l):
    
    return np.array([[1.,l],[0.,1.]])

#reflection (flat interface)
def rfl_fi():
    
    return np.array([[1.,0.],[0.,1.]])

#refraction (flat interface)
def rfr_fi(n1,n2):
    
    return np.array([[1.,0.],[0.,n1/n2]])



# NOTE: *** APPROXIMATION *** (really bad)
#   The QWP is considered as a piece of glass 


# initialize the number of optical path segments inside the system
# or equvalently the number of surfaces (included input surface)

n_segm = 11

n_segm += 1


# storage arrays

#optical path lengths (included 'null' length input beam)
ops = np.zeros(n_segm)
# refraction indeces
n_ri = np.zeros(n_segm+1)
# surface properties
surf_tt = np.array(np.zeros(n_segm),dtype=bool)

# ABCD matrices
abcd_m = np.zeros([2*n_segm,2,2])




# NOTA: *** ATTENTION ***
# the following abcd matrix + vector formalism is only valid in paraxial approximation 
# that is:  cos(alpha)=1, sin(alpha)=theta




# initialize initial beam

r_0 = 0. #mm
alpha_0 = 0.1/180.*np.pi

v_0 = np.array([r_0,alpha_0])


# initialize optical path lengths 
# first model: compute them for perfectly aligned input beam

xy_in = 2.5 #mm
a30 = 30./180.*np.pi #rad
a60 = 60./180.*np.pi #rad
l_pbs = 10. #mm
l_qwp = 1. #mm
l_out = 5. #mm

#NOTE: they can be better parametrized
ops = np.array([0., xy_in*np.tan(a30), 2*xy_in*np.tan(a30), 2*xy_in*np.cos(a30), l_pbs, l_qwp, l_qwp, l_pbs-xy_in , xy_in, xy_in*np.tan(a60), 2*xy_in*np.tan(a30), xy_in*np.tan(a30)])

# initialize refraction indeces (corresponding to the optical segments (figure notation)
# + external n)

n_ri = np.array([1.,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.5,1.])

# initialize the surface properties:
    # TRUE = Transmission
    # FALSE = Reflection

surf_tt[0] = True
surf_tt[1] = True
surf_tt[2] = False
surf_tt[3] = True
surf_tt[4] = True
surf_tt[5] = False
surf_tt[6] = True
surf_tt[7] = False
surf_tt[8] = True
surf_tt[9] = False
surf_tt[10] = True
surf_tt[11] = True


# populate the abcd matrices storage array

for n in range(n_segm):
    
    # n segment propagation
    abcd_m[2*n,:,:] = propag(ops[n])
    
    # n surface
    if surf_tt[n] == True:
        
        abcd_m[2*n+1,:,:] = rfr_fi(n_ri[n], n_ri[n+1])
        
    if surf_tt[n] == False:
        
        abcd_m[2*n+1,:,:] = rfl_fi()


# perform matrix multiplication
# pay attention that elements must be multiplied from last to first

abcd_syst = abcd_m[0,:,:]

for n in range(1,2*n_segm):
    
    abcd_syst = np.matmul(abcd_m[n,:,:],abcd_syst) 
    

# compute the exit beam vector

v_out = np.matmul(abcd_syst,v_0)

print(v_out)



